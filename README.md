# teste web jump

## versão do PHP 7.2.26
> php -S localhost:8090

## banco utilizado SQLite 3
> core\db\database.file

## endpoints criados
==> segue na raíz do repositório o arquivo para configurar o Insomnia 
> insomnia.json

### atributos necessários para chamada da API
os atributos são necessários serem enviados mas não passam por nenhuma verificação:
    - auth: **bearer token** com _prefix_
    - header _application/json_
    - header _pass-token_

### produto
- **GET**
  - lista todos os produtos
  > /product
  - busca um determinado produto
  > /product/:sku
- **POST**
  - cria um novo produto
  > /product
    - campos esperados:  
        - **nome**            : _string_
        - **sku**             : _string_
        - **descricao**       : _string_
        - **quantidade**      : _inteiro_
        - **preco**           : _numérico_
        - **categoria**       : _string_ 
- pesquisar por produto 
  - cria um novo produto
  > /product/search
    - pesquisar produto por nome ou descrição:  
        - **term**            : _string_
- **PUT**
  - alterar um produto
  > /product/:sku
    - campos esperado _um ou mais_:
        - **nome**            : _string_
        - **sku**             : _string_
        - **descricao**       : _string_
        - **quantidade**      : _inteiro_
        - **preco**           : _numérico_
        - **categoria**       : _string_ 
- **DEL**
  - exclui um produto
  > /product/:sku


### categoria
- **GET**
  - lista todos as categorias
  > /category
  - busca uma determinada categoria
  > /category/:codigo
- **POST**
  - cria um novo categoria
  > /category
    - campos esperados:  
        - **nome**            : _string_
        - **codigo**          : _string_
- pesquisar por categoria 
  - cria uma nova categoria
  > /category/search
    - pesquisar categoria por nome
        - **term**            : _string_
- **PUT**
  - alterar um categoria
  > /category/:codigo
    - campos esperado _um ou mais_:
        - **nome**            : _string_
        - **codigo**          : _string_
- **DEL**
  - exclui uma categoria
  > /category/:codigo
  